#importa a biblioteca microbit
from microbit import *

while True:
    #mensagem aparecendo no display
    display.scroll('Horta')
    #pausa por x milesimos de segundos
    sleep(2000) 
    
    #leitura de valor no pino 0
    reading = pin0.read_analog()
    #calcula percentual de umidade
    moisture = 100.0-((reading*100.0)/1023.0)
    #mostra o percentual de umidade no display
    display.scroll('Um: %.1f' % moisture + ' %')
    
    #teste do limiar de umidade
    if (moisture < 50.0):
        #envia sinal para ligar irrigacao
        pin1.write_digital(1)
        sleep(500)
        #envia sinal para desligar irrigacao
        pin1.write_digital(0)
        sleep(500)
        
    #leitura da temperatura ambiente
    temp = temperature()
    display.scroll('T: %d' % temp + ' C')
    sleep(3000)
    