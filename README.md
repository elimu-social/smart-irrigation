## Irrigação automatizada
> Este projeto utiliza a placa BBC Micro:bit para controle de umidade com irrigação automática utilizando sensor de umidade de solo LM393.
### Estrutura de arquivos
1. <microbit-code.py> script python para Micro:bit, transcrição do código em blocos.
2. <microbit-Smart-irrigation.hex> arquivo com código em blocos para Micro:bit.
3. <irrigation.ino> script para projeto usando Arduino (outros sensores foram utilizados neste projeto).

### Para simples uso deste projeto
1. Fazer o download dos arquivos de código deste projeto.
2. Acessar a [interface de progração do Micro:bit](https://microbit.org/code/) (ou Arduino).
3. Conectar à placa.
4. Realizar upload da programação.

### Simples descrição do circuito
1. Pino zero (P0) -> leitura do sensor de umidade de solo.
2. Pino um (P1) -> controle on/off utilizando módulo relé.
3. 3V e GND para alimentação do sensor.
4. Alimentação da bomba por fonte externa 5V CC.
5. Uma [descrição visual](https://www.canva.com/design/DAEtNc60ZqQ/62565xUvvkyCUwVq_M2pdw/view?utm_content=DAEtNc60ZqQ&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton) pode ser acessada.



